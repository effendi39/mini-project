<?php


namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class Perusahaan extends Model
{
    protected $table = 'perusahaan';

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
        "id",
        "nama_perusahaan",
        "alamat",
        "pic",
        "no_tlp",
        "email"
    ];

}
