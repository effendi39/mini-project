<?php


namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class Event extends Model
{
    protected $table = 'event';

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
        "id",
        "nama_event",
        "keterngan",
        "tgl_mulai",
        "tgl_selesai",
        "harga"
    ];

}
