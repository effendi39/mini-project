<?php

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\SocialMedia;
use App\Route;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'no_hp'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }


    public function roles()
    {
        return $this->belongsToMany('App\Role', 'App\RoleUser', 'user_id', 'role_id');
    }

    public function routes()
    {
        return $this->belongsToMany('App\User', 'App\RouteUser', 'user_id', 'role_id');
    }

    public function events()
    {
        return $this->belongsToMany('App\Event', 'App\EventPeserta', 'event_id', 'perusahaan_id');
    }

    public function social_media()
    {
        return $this->hasMany(SocialMedia::class);
    }

    public function perusahaan()
    {
        return $this->hasOne('App\Perusahaan');
    }

    public function cek_route($routename)
    {
        $route_id = Route::where('route_name', $routename)->first()->id;
        // dd($route_id);
        $roles = $this->roles;
        // dd($roles);
        $cek = false;
        if($roles){
            foreach($roles as $role){
                foreach($role->route as $route){
                    if($route->id == $route_id){
                        $cek = true;
                    }else{
                        $cek = false;
                    }
                }
            }
        }
        return $cek;
    }
    
}
