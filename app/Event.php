<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table = 'events';

    protected $guarded = ['id'];


    public function perusahaan()
    {
        return $this->belongsToMany('App\Event', 'App\EventPerusahaan', 'perusahaan_id', 'event_id');
    }

    public function user()
    {
        return $this->belongsToMany('App\User', 'App\EventPeserta', 'perusahaan_id', 'event_id');
    }
}
