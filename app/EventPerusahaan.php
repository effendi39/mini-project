<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventPerusahaan extends Model
{
    protected $guarded = ['id'];
    protected $table = 'event_perusahaan';
}
