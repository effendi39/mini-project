<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perusahaan extends Model
{
    protected $guarded = ['id'];

    protected $table = 'perusahaan';


    public function events()
    {
        return $this->belongsToMany('App\Perusahaan', 'App\EventPerusahaan', 'event_id', 'perusahaan_id');
    }

    public function user()
    {
        return $this->hasOne('App\User');
    }
}
