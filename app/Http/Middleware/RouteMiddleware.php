<?php

namespace App\Http\Middleware;

use Closure;
use Tymon\JWTAuth\JWTAuth;

class RouteMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $routename = \Route::current()->getName();
        // dd($routename);
        $user = auth()->user();
        // dd($user);
        if($user->cek_route($routename)){
            return $next($request);
        }
        return response()->json(['error_message' => 'Halaman Terlarang'], 403);
    }
}
