<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RoleRequest;
use App\Role;
use JWTAuth;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleRequest $request)
    {
        $data =[];
        $role = Role::create([
            'role_name' => $request->role_name
        ]);
        
        $newToken = auth()->refresh();
        $data['token'] = $newToken;
        $data['role'] = $role;

        return response()->json([
            'response_code' => '00',
            'response_message' => 'role berhasil ditambahkan',
            'data'      => $data
        ], 200);


    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RoleRequest $request, $id)
    {
        // dd($id);
        $data =[];
        $role = Role::where('id', $id)->update([
            'role_name' => $request->role_name
        ]);
        
        $newToken = auth()->refresh();
        $data['token'] = $newToken;
        $data['role'] = $role;

        return response()->json([
            'response_code' => '00',
            'response_message' => 'role berhasil diupdate',
            'data'      => $data
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
