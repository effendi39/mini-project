<?php

namespace App\Http\Controllers;

use App\EventPerusahaan;
use App\EventPeserta;
use App\Http\Requests\DaftarEventRequest;

class DaftarEventController extends Controller
{
    public function perusahaan(DaftarEventRequest $request)
    {
        $data =[];
        $daftarPerusahaan = EventPerusahaan::create([
            'event_id'  =>$request->event_id,
            'perusahaan_id'  =>$request->perusahaan_id,
        ]);

        $newToken = auth()->refresh();
        $data['token'] = $newToken;
        $data['daftarPerusahaan'] = $daftarPerusahaan;

        return response()->json([
            'response_code' => '00',
            'response_message' => 'Perusahaan berhasil mendaftar ke event ini',
            'data'      => $data
        ], 200);
    }

    public function peserta(DaftarEventRequest $request)
    {
        $data =[];
        $daftarPeserta = EventPeserta::create([
            'event_id'  =>$request->event_id,
            'user_id'  =>$request->user_id,
        ]);

        $newToken = auth()->refresh();
        $data['token'] = $newToken;
        $data['daftarPeserta'] = $daftarPeserta;

        return response()->json([
            'response_code' => '00',
            'response_message' => 'Peserta berhasil mendaftar ke event ini',
            'data'      => $data
        ], 200);
    }
}
