<?php

<<<<<<< HEAD

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\perusahaan;

use Illuminate\Support\Facades\Validator;
use DB;

class PerusahaanController extends Controller
{


    public function index()
    {

        $perusahaan = Perusahaan::get();
        return view('backend.perusahaan.index', compact('perusahaan'));
    }

    public function getData()
    {
        $perusahaan = Perusahaan::get();

        if($perusahaan) {
            return response()->json([
                'status'=>'oke',
                'data' => $perusahaan
            ]);
        } else {
            return response()->json(['status'=>'insert_failed']);
        }
    }


    private function validateRequest($request, $id=0){
        $messages = [
            'required' => 'Kolom <b>:attribute</b> harus diisi.',

        ];
        return Validator::make($request->all(), [
            "nama_perusahaan" => "required"
        ], $messages);
    }


    public function store(Request $request)
    {
        if($request->ajax()){
            if ($this->validateRequest($request)->fails()) {

                return response()->json([
                    'status'=>'insert_failed',
                    'error' => $this->validateRequest($request)->messages()
                ]);

            }


            DB::beginTransaction();

            try {
                $insertRole = DB::table('perusahaan')->insertGetId([
                    "nama_perusahaan" => $request->nama_perusahaan,
                    "alamat" => $request->alamat,
                    "pic" => $request->pic,
                    "no_tlp" => $request->no_tlp,
                    "email" => $request->email
                ]);

                DB::commit();
                return response()->json(['status'=>'insert_successful']);

            } catch (\Throwable $e) {
                DB::rollback();
                throw $e;
                return response()->json(['status'=>'insert_failed']);

            }

        } else {
            return redirect('/');
        }

    }



=======
namespace App\Http\Controllers;

use App\Perusahaan;
use App\Http\Requests\PerusahaanRequest;
use Illuminate\Support\Facades\Auth;

class PerusahaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PerusahaanRequest $request)
    {
        $perusahaan = Perusahaan::create([
            'nama_perusahaan'   => $request->nama_perusahaan,
            'alamat'   => $request->alamat,
            'penanggung_jawab'   => Auth::user()->id,
        ]);

        $newToken = auth()->refresh();
        $data['token'] = $newToken;
        $data['perusahaan'] = $perusahaan;

        return response()->json([
            'response_code' => '00',
            'response_message' => 'Perusahaan berhasil ditambahkan',
            'data'      => $data
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

>>>>>>> 6b884b257fff867d8b647bc6f28951d0b5a64642
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
<<<<<<< HEAD

    public function update(Request $request, $id)
    {
        // return $request->all();
        if($request->ajax()){
            if ($this->validateRequest($request, $id)->fails()) {

                return response()->json([
                    'status'=>'insert_failed',
                    'error' => $this->validateRequest($request, $id)->fails()
                ]);

            }
            DB::beginTransaction();

            try {
                $insertRole = Role::find($id)->update([
                    "nama_perusahaan" => $request->nama_perusahaan,
                    "alamat" => $request->alamat,
                    "pic" => $request->pic,
                    "no_tlp" => $request->no_tlp,
                    "email" => $request->email
                ]);

                DB::commit();
                return response()->json(['status'=>'insert_successful']);

            } catch (\Throwable $e) {
                DB::rollback();
                throw $e;
                return response()->json(['status'=>'insert_failed']);

            }



            if($updateRole) {
                return response()->json(['status'=>'insert_successful']);
            } else {
                return response()->json(['status'=>'insert_failed']);
            }
        } else {
            return redirect('/');
        }

    }


    public function destroy($id)
    {
        $query = Role::find($id)->delete();

        if($query) {
            return response()->json(['status'=>'delete_successful']);
        } else {
            return response()->json(['status'=>'delete_failed']);
        }

    }

    public function deleteData(Request $request, $id)
    {

        if($request->ajax()){
            $query = Role::find($id)->delete();
            $modul = DB::table('perusahaan')->where('id',$id)->delete();
            if($query) {
                return response()->json(['status'=>'delete_successful']);
            } else {
                return response()->json(['status'=>'delete_failed']);
            }
        } else {
            return redirect('/');
        }
=======
    public function update(PerusahaanRequest $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
>>>>>>> 6b884b257fff867d8b647bc6f28951d0b5a64642
    }
}
