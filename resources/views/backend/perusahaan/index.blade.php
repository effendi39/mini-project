@extends('backend.layout.default')

@push('style')

@endpush

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">DataTable with minimal features & hover style</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <table id="perusahaan_datatable" class="table table-bordered table-hover">
              
            </table>
        </div>
    <!-- /.card-body -->
    </div>
    <!-- /.card -->
@endsection

@push('scripts')
<!-- Aditional Scripts Here -->
<script src="{{ asset('additional/js/perusahaan.js') }}"></script>
@endpush