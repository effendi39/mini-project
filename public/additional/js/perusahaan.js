var data_table;
$(document).ready(function () {
    loadData();

    $('button#tambah').on('click', function () {
        clearModal();
        $('#modal_label').text('Form Tambah Role');
        $('#method_field').val("POST");
        $(".modal-form").modal('show');
    });

    $('i#btn_simpan').on('click', function () {
        insertUpdateProses();
    });
});

function loadData() {
    data_table = $('#perusahaan_datatable').DataTable({
        processing: true,
        ajax: {
            "url": "" + base_url + '/getDataJson/perusahaan',
            'type': 'GET',
            'dataType': 'JSON',
            'error': function (xhr, textStatus, ThrownException) {
                alert('Error loading data. Exception: ' + ThrownException + "\n" + textStatus);
            }
        },
        columns: [{
            title: "ID ",
            data: "id",
            visible: false,
            sortable: true,
            class: "text-center"
        }, {
            title: "Nama",
            data: "nama_perusahaan",
            visible: true,
            sortable: true,
            class: "text-center"
        }, {
            title: "PIC",
            data: "pic",
            visible: true,
            sortable: true,
            class: ""
        },{
            title: "Alamat",
            data: "alamat",
            visible: true,
            sortable: true,
            class: ""
        }]
    });
}

function insertUpdateProses() {
    var form = $('#form_perusahaan');
    if (form.valid() == true) {

        var method = $('#method_field').val();
        var action_url = "" + base_url + "/role";
        var action_type = "Tambah";
        if (method === "PUT") {
            action_url = "" + base_url + "/role/" + $('#role_id').val();
            action_type = "Ubah";
        }
        $.ajax({
            type: 'POST',
            url: action_url,
            dataType: 'JSON',
            data: form.serialize(),
            beforeSend: function () {
                sweetAlertLoading('Memproses');
            },
            success: function (data) {
                if (data.status == 'insert_successful') {
                    sweetAlertDefault('<b>Berhasil ' + action_type + ' Data</b>', 'success', 2000 );

                    $('.modal-form').modal('toggle');
                    data_table.ajax.reload(null, false);
                } else if (data.status == 'insert_failed') {
                    sweetAlertDefault('<b>Gagal ' + action_type + ' </b>', 'error', 2000 );

                    var errors = data.error;
                    errorValidationLaravel(errors, '#error-validation');

                } else {
                    sweetAlertDefault('<b>Gagal ' + action_type + ' (Kesalahan Sistem) </b>', 'error', 2000 );
                }
            },
            error: function (xmlhttprequest, textstatus, message) {
                sweetAlertDefault('<b>Koneksi Ke Server Gagal, Mohon Refresh Halaman</b>', 'error', 2000 );
            }
        });

    } else {
        sweetAlertLoading('Mohon Isi Form Dengan Lengkap, Cek Input Form Yang Berwarna Merah',1000);
    }
}

