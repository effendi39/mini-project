<?php

use Illuminate\Http\Request;
// use Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::get('/register', function(){
//     return view('auth.register');
// });
// Route::get('/login', function(){
//     return view('auth.login');
// });


// Route::post('register/proses', 'RegisterController@proses');
// Route::post('login/proses', 'LoginController@proses');

// Route::middleware('auth:api')->group(function(){
//     Route::post('/logout', 'LoginController@logout');
// });
// Route::get('/home', 'HomeController@index')->name('home');


// Route::get('/login/{provider}', 'LoginController@redirectToProvider');
// Route::get('/login/{provider}/callback', 'LoginController@handleProviderCallback');


Route::group([
    'middleware'    => 'api',
    'prefix'        => 'auth',
    'namespace'     => 'Auth'
], function($router){
    Route::post('/register', 'RegisterController')->name('register');
    Route::post('login', 'LoginController')->name('login');
    Route::get('social/{provider}', 'SocialiteController@redirectToProvider')->name('register_sosmed');
    Route::get('social/{provider}/callback', 'SocialiteController@handleProviderCallback')->name('register_sosmed_callback');
});


Route::group([
    'middleware'       => 'api'
], function($route){
    Route::post('role', 'RoleController@store')->name('role.store');
    Route::post('route', 'RouteController@store')->name('route.store');
    Route::post('event', 'EventController@store')->name('event.store');
    Route::post('perusahaan', 'PerusahaanController@store')->name('perusahaan.store');
    Route::post('daftar-event-perusahaan', 'DaftarEventController@perusahaan')->name('event_perusahaan.store');
    Route::post('daftar-event-peserta', 'DaftarEventController@peserta')->name('event_peserta.store');
});
Route::patch('role/{role}/update', 'RoleController@update')->name('role.update');